package com.domaci.todo_vezba.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "grupa")
public class Grupa {

    @DatabaseField(columnName = "id", generatedId = true)
    private int id;
    @DatabaseField(columnName = "naziv")
    private String naziv;
    @DatabaseField(columnName = "datum")
    private String datum;
    @DatabaseField(columnName = "oznaka")
    private String oznaka;
    @ForeignCollectionField(columnName = "grupa", eager = true)
    private ForeignCollection<TODOZadatak> todoZadaci;

    public Grupa() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getOznaka() {
        return oznaka;
    }

    public void setOznaka(String oznaka) {
        this.oznaka = oznaka;
    }

    public ForeignCollection<TODOZadatak> getTodoZadaci() {
        return todoZadaci;
    }

    public void setTodoZadaci(ForeignCollection<TODOZadatak> todoZadaci) {
        this.todoZadaci = todoZadaci;
    }

    @Override
    public String toString() {
        return naziv;
    }
}

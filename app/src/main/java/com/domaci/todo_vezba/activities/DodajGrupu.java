package com.domaci.todo_vezba.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.domaci.todo_vezba.R;
import com.domaci.todo_vezba.db.DatabaseHelper;
import com.domaci.todo_vezba.model.Grupa;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DodajGrupu extends AppCompatActivity {

    private EditText etNaziv;
    private EditText etOznaka;
    private Button btnCancel;
    private Button btnSave;

    private Date currentTime;

    private SharedPreferences sharedPreferences;
    private DatabaseHelper databaseHelper;

    public static String TOAST_SETTINGS = "toast_settings_cb";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_grupu);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        etNaziv = findViewById(R.id.etNaziv);
        etOznaka = findViewById(R.id.etOznake);
        btnCancel = findViewById(R.id.btnCancel);
        btnSave = findViewById(R.id.btnSave);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateGrupa(etNaziv) && validateGrupa(etOznaka)) {
                    dodajGrupu();
                }
                Intent intent = new Intent(DodajGrupu.this, MainActivity.class);
                startActivity(intent);
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToast("Odustali ste od dodavanja grupe");
                Intent intent = new Intent(DodajGrupu.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    private void dodajGrupu() {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); //u ovom foramtu ce se ispisivati datum
        currentTime = Calendar.getInstance().getTime(); //vraca tremutno vreme za upis u bazu
        dateFormat.format(currentTime); //formatira i vraca string

        Grupa grupa = new Grupa();

        grupa.setNaziv(etNaziv.getText().toString());
        grupa.setOznaka(etOznaka.getText().toString());
        grupa.setDatum(dateFormat.format(currentTime));
        try {
            getDatabaseHelper().getGrupaDao().create(grupa);
            showToast("Grupa dodata");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private boolean validateGrupa (EditText editText) {
        String titleInput = editText.getText().toString().trim();

        if (titleInput.isEmpty()) {
            editText.setError("Polje ne moze biti prazno!");
            return false;
        }else {
            editText.setError(null);
            return true;
        }
    }

    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private void showToast(String message) {
        boolean toast = sharedPreferences.getBoolean(TOAST_SETTINGS, false);
        if (toast) Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}

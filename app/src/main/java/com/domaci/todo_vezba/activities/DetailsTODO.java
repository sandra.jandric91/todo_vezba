package com.domaci.todo_vezba.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.domaci.todo_vezba.AboutDijalog;
import com.domaci.todo_vezba.R;
import com.domaci.todo_vezba.db.DatabaseHelper;
import com.domaci.todo_vezba.model.TODOZadatak;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DetailsTODO extends AppCompatActivity {

    private TextView etNazivDetaljiTODO;
    private TextView etOpisDetaljiTODO;
    private TextView etStatusDetaljiTODO;
    private TextView etDatumKreiranjaDetaljiTODO;
    private TextView etDatumZavrsetkaDetaljiTODO;
    private TextView etPrioritetDetaljiTODO;

    private DatabaseHelper databaseHelper;
    private TODOZadatak todo;

    private Toolbar toolbar;
    private List<String> drawerItems;
    private ActionBarDrawerToggle drawerToggle;
    private AlertDialog aboutDijalog;

    private AlertDialog dijalogBrisanje;
    private SharedPreferences sharedPreferences;

    public static String TOAST_SETTINGS = "toast_settings_cb";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_todo);

        setupToolbar();
        fillDrawerWithItems();
        setupDrawer();
        showDetails();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_action_home);
            actionBar.show();
        }
    }

    private void fillDrawerWithItems() {
        drawerItems = new ArrayList<>();
        drawerItems.add("Sve grupe");
        drawerItems.add("Podesavanja");
        drawerItems.add("O aplikaciji");
    }

    private void setupDrawer() {
        final LinearLayout linearZaDrawer = findViewById(R.id.linear_za_drawerList);
        final DrawerLayout masterDrawerLayout = findViewById(R.id.masterDrawerLayout);
        ListView lvDrawer = findViewById(R.id.lvDrawer);
        lvDrawer.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, drawerItems));
        lvDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String naslov = "nepoznato";
                switch (position) {
                    case 0:
                        naslov = "Sve grupe";
                        break;
                    case 1:
                        naslov = "Podesavanja";
                        showSettings();
                        break;
                    case 2:
                        naslov = "O aplikaciji";
                        showDijalog();
                }
                setTitle(naslov);
                masterDrawerLayout.closeDrawer(linearZaDrawer);
            }
        });
        drawerToggle = new ActionBarDrawerToggle(this, masterDrawerLayout, toolbar, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View view) {
                invalidateOptionsMenu();
            }
        };
        masterDrawerLayout.closeDrawer(linearZaDrawer);
    }

    private void showSettings() {
        Intent intent = new Intent(this, PrefsActivity.class);
        startActivity(intent);
    }

    private void showDijalog() {
        if (aboutDijalog == null) {
            aboutDijalog = new AboutDijalog(this).prepareDialog();
        } else {
            if (aboutDijalog.isShowing()) {
                aboutDijalog.dismiss();
            }
        }
        aboutDijalog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_details_todo, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                showDijalogBrisanje();
                break;
            case R.id.action_close:
                showDijalogClose();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public class BrisanjeDijalog extends AlertDialog.Builder {

        public BrisanjeDijalog(@NonNull Context context) {
            super(context);
            setTitle("Obrisi TODO");
            setMessage("Da li zelite da obrisete TODO?");
            setPositiveButton("U redu", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    obrisiTODO();
                }
            });
            setNegativeButton("Odustani", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }

        public AlertDialog prepareDialog() {
            AlertDialog alertDialog = create();
            alertDialog.setCanceledOnTouchOutside(false);
            return alertDialog;
        }
    }

    private void showDijalogBrisanje() {
        if (dijalogBrisanje == null) {
            dijalogBrisanje = new BrisanjeDijalog(this).prepareDialog();
        } else {
            if (dijalogBrisanje.isShowing()) {
                dijalogBrisanje.dismiss();
            }
        }
        dijalogBrisanje.show();
    }

    private void obrisiTODO() {
        if (todo != null) {
            try {
                getDatabaseHelper().getTodoDao().delete(todo);
                showToast("TODO obrisan");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        onBackPressed();
    }

    public class CloseTODODijalog extends AlertDialog.Builder {

        public CloseTODODijalog(@NonNull Context context) {
            super(context);
            setTitle("Zatvori TODO");
            setMessage("Da li zelite da zatvorite TODO?");
            setPositiveButton("U redu", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    closeTODO();
                }
            });
            setNegativeButton("Odustani", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }

        public AlertDialog prepareDialog() {
            AlertDialog alertDialog = create();
            alertDialog.setCanceledOnTouchOutside(false);
            return alertDialog;
        }
    }

    private void showDijalogClose() {
        if (dijalogBrisanje == null) {
            dijalogBrisanje = new CloseTODODijalog(this).prepareDialog();
        } else {
            if (dijalogBrisanje.isShowing()) {
                dijalogBrisanje.dismiss();
            }
        }
        dijalogBrisanje.show();
    }

    private void closeTODO() {
        if (todo != null) {
            if (!todo.getStatus().equalsIgnoreCase("Zavrsen")) {
                todo.setStatus("Zavrsen");
                try {
                    getDatabaseHelper().getTodoDao().update(todo);
                    showToast("TODO zavrsen");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        finish();
    }

    private void showDetails() {

        etNazivDetaljiTODO = findViewById(R.id.etNazivDetaljiTODO);
        etOpisDetaljiTODO = findViewById(R.id.etOpisDetaljiTODO);
        etStatusDetaljiTODO = findViewById(R.id.etStatusDetaljiTODO);
        etDatumKreiranjaDetaljiTODO = findViewById(R.id.etDatumKreiranjaTODO);
        etDatumZavrsetkaDetaljiTODO = findViewById(R.id.etDatumZavrsetkaTODOTODO);
        etPrioritetDetaljiTODO = findViewById(R.id.etPrioritetDetaljiTODO);

        int todoID = getIntent().getExtras().getInt("todo_id");

        try {
            todo = getDatabaseHelper().getTodoDao().queryForId(todoID);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        etNazivDetaljiTODO.setText(todo.getNaziv());
        etOpisDetaljiTODO.setText(todo.getOpis());
        etStatusDetaljiTODO.setText(todo.getStatus());
        etDatumKreiranjaDetaljiTODO.setText(todo.getDatumKreiranja());
        etDatumZavrsetkaDetaljiTODO.setText(todo.getDatumZavrsetka());
        etPrioritetDetaljiTODO.setText(todo.getPrioritet());
    }

    private void showToast(String message) {
        boolean toast = sharedPreferences.getBoolean(TOAST_SETTINGS, false);
        if (toast) Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}

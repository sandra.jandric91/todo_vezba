package com.domaci.todo_vezba.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.domaci.todo_vezba.R;
import com.domaci.todo_vezba.db.DatabaseHelper;
import com.domaci.todo_vezba.model.Grupa;
import com.domaci.todo_vezba.model.TODOZadatak;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DodajTODOZadatak extends AppCompatActivity {

    EditText etNazivTODO;
    EditText etOpis;
    EditText etZavrsetakZadatka;
    private RadioGroup radioGroup;
    private RadioButton priorityRadioButton;
    Button btnCancel;
    Button btnAdd;

    private DatabaseHelper databaseHelper;
    private Date currentTime;
    private Grupa grupa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_todozadatak);

        etNazivTODO = findViewById(R.id.etNazivTODO);
        etOpis = findViewById(R.id.etOpisTODO);
        radioGroup = findViewById(R.id.radio_group);
        etZavrsetakZadatka = findViewById(R.id.etDatumZavrsetkaTODO);

        btnCancel = findViewById(R.id.btnCancel);
        btnAdd = findViewById(R.id.btnAdd);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateTODO(etNazivTODO) && validateTODO(etOpis) && validateTODO(etZavrsetakZadatka)) {
                    checkRadioButton();
                    addTODOtoList();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void checkRadioButton(){
        int radioId = radioGroup.getCheckedRadioButtonId();
        priorityRadioButton = findViewById(radioId);
    }

    private void addTODOtoList() {

        int grupaID = getIntent().getExtras().getInt("grupa_id");

        try {
            grupa = getDatabaseHelper().getGrupaDao().queryForId(grupaID);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); //u ovom foramtu ce se ispisivati datum
        currentTime = Calendar.getInstance().getTime(); //vraca tremutno vreme za upis u bazu
        dateFormat.format(currentTime); //formatira i vraca string

        Calendar now = Calendar.getInstance();
        now.add(Calendar.MINUTE, Integer.parseInt(etZavrsetakZadatka.getText().toString()));


        TODOZadatak todoZadatak = new TODOZadatak();

        todoZadatak.setNaziv(etNazivTODO.getText().toString());
        todoZadatak.setOpis(etOpis.getText().toString());
        todoZadatak.setPrioritet(priorityRadioButton.getText().toString());
        todoZadatak.setDatumKreiranja(dateFormat.format(currentTime));
        todoZadatak.setDatumZavrsetka(dateFormat.format(now.getTime()));
        todoZadatak.setStatus("Aktivan");
        todoZadatak.setGrupa(grupa);

        try {
            getDatabaseHelper().getTodoDao().create(todoZadatak);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(DodajTODOZadatak.this, DetailsActivity.class);
        intent.putExtra("grupa_id", grupaID);
        startActivity(intent);
    }

    private boolean validateTODO(EditText editText) {
        String titleInput = editText.getText().toString().trim();

        if (titleInput.isEmpty()) {
            editText.setError("Field can't be empty");
            return false;
        }else {
            editText.setError(null);
            return true;
        }
    }

    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(DodajTODOZadatak.this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        //nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}

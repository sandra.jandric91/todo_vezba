package com.domaci.todo_vezba.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.domaci.todo_vezba.AboutDijalog;
import com.domaci.todo_vezba.R;
import com.domaci.todo_vezba.adapteri.RVAdapter;
import com.domaci.todo_vezba.db.DatabaseHelper;
import com.domaci.todo_vezba.model.Grupa;
import com.domaci.todo_vezba.model.TODOZadatak;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements RVAdapter.OnRVClickedListener {

    private Toolbar toolbar;
    private SharedPreferences sharedPreferences;
    private List<String> drawerItems;
    private ActionBarDrawerToggle drawerToggle;
    private AlertDialog aboutDijalog;

    private DatabaseHelper databaseHelper;

    private List<Grupa> listaGrupa;
    private RecyclerView recyclerView;
    private RVAdapter rvAdapter;

    private TODOZadatak todo;
    private TextView etDatumZavrsetkaDetaljiTODO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupToolbar();
        fillDrawerWithItems();
        setupDrawer();
        showList();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_action_home);
            actionBar.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                dodajGrupu();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void dodajGrupu() {

        Intent intent = new Intent(this, DodajGrupu.class);
        startActivity(intent);
    }

    private void fillDrawerWithItems() {
        drawerItems = new ArrayList<>();
        drawerItems.add("Sve grupe");
        drawerItems.add("Podesavanja");
        drawerItems.add("O aplikaciji");
    }

    private void setupDrawer() {
        final LinearLayout linearZaDrawer = findViewById(R.id.linear_za_drawerList);
        final DrawerLayout masterDrawerLayout = findViewById(R.id.masterDrawerLayout);
        ListView lvDrawer = findViewById(R.id.lvDrawer);
        lvDrawer.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, drawerItems));
        lvDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String naslov = "nepoznato";
                switch (position) {
                    case 0:
                        naslov = "Sve grupe";
                        showList();
                        break;
                    case 1:
                        naslov = "Podesavanja";
                        showSettings();
                        break;
                    case 2:
                        naslov = "O aplikaciji";
                        showDijalog();
                }
                setTitle(naslov);
                masterDrawerLayout.closeDrawer(linearZaDrawer);
            }
        });
        drawerToggle = new ActionBarDrawerToggle(this, masterDrawerLayout, toolbar, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View view) {
                invalidateOptionsMenu();
            }
        };
        masterDrawerLayout.closeDrawer(linearZaDrawer);
    }

    public void showList() {
        try {
            listaGrupa = getDatabaseHelper().getGrupaDao().queryForAll();
            refresh();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (listaGrupa == null) {
            listaGrupa = new ArrayList<>();
        }

        recyclerView = findViewById(R.id.rvListaAtrakcija);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true); //ubacuje na pocetak prvi element a ne na dno
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(new RVAdapter(listaGrupa, this));
    }


    private void refresh() {
        if (recyclerView != null) {
            rvAdapter = (RVAdapter) recyclerView.getAdapter();
            if (rvAdapter != null) {
                try {
                    listaGrupa = getDatabaseHelper().getGrupaDao().queryForAll();
                    rvAdapter.notifyDataSetChanged();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void showSettings() {
        Intent intent = new Intent(this, PrefsActivity.class);
        startActivity(intent);
    }

    private void showDijalog() {
        if (aboutDijalog == null) {
            aboutDijalog = new AboutDijalog(this).prepareDialog();
        } else {
            if (aboutDijalog.isShowing()) {
                aboutDijalog.dismiss();
            }
        }
        aboutDijalog.show();
    }

    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        refresh();
    }

    @Override
    public void onRVClicked(Grupa grupa) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("grupa_id", grupa.getId());
        startActivity(intent);
    }
}

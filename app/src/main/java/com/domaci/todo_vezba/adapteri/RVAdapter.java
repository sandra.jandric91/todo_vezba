package com.domaci.todo_vezba.adapteri;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.domaci.todo_vezba.R;
import com.domaci.todo_vezba.model.Grupa;

import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.MyViewHolder> {

    public List<Grupa> listaGrupa;
    public OnRVClickedListener listener;

    public interface OnRVClickedListener{
        void onRVClicked(Grupa grupa);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitleRV;
        private TextView tvDatum;
        private View view;

        public MyViewHolder (View itemView) {
            super(itemView);
            view = itemView;
            tvTitleRV = itemView.findViewById(R.id.tvTitleRV);
            tvDatum = itemView.findViewById(R.id.tvDatumRV);
        }

        public void bind(final Grupa grupa, final OnRVClickedListener listener) {
            tvTitleRV.setText(grupa.getNaziv());
            tvDatum.setText(grupa.getDatum());
            /*Picasso.get()
                    .load("http://becomingminimalist.com/wp-content/uploads/2008/07/post-it-note.jpg?w=300")
                    .resize(30, 30)
                    .into(imageViewRV);*/
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRVClicked(grupa);
                }
            });
        }
    }

    public RVAdapter(List<Grupa> grupeLista, OnRVClickedListener listener) {
        this.listaGrupa = grupeLista;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_single_item, viewGroup, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.bind(listaGrupa.get(i), listener);
        int currentPostition = i;
        final Grupa grupa = listaGrupa.get(currentPostition);
    }

    @Override
    public int getItemCount() {
        return listaGrupa.size();
    }
}

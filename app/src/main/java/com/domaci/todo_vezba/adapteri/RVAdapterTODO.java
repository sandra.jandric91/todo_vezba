package com.domaci.todo_vezba.adapteri;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.domaci.todo_vezba.R;
import com.domaci.todo_vezba.model.TODOZadatak;

import java.util.List;

public class RVAdapterTODO extends RecyclerView.Adapter<RVAdapterTODO.MyViewHolder> {

    public List<TODOZadatak> listaTODO;
    public OnRVTODOClickedListener listener;

    public interface OnRVTODOClickedListener {
        void onRVTODOClicked(TODOZadatak todoZadatak);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitleRVTODO;
        private TextView tvDatumRVTODO;
        private TextView tvDatumZavrsetkaRVTODO;
        private View view;

        public MyViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tvTitleRVTODO = itemView.findViewById(R.id.tvTitleTODORV);
            tvDatumRVTODO = itemView.findViewById(R.id.tvDatumRVTODO);
            tvDatumZavrsetkaRVTODO = itemView.findViewById(R.id.tvDatumZavrsetkaRVTODO);
        }

        public void bind(final TODOZadatak todoZadatak, final OnRVTODOClickedListener listener) {
            tvTitleRVTODO.setText(todoZadatak.getNaziv());
            tvDatumRVTODO.setText(todoZadatak.getDatumKreiranja());
            tvDatumZavrsetkaRVTODO.setText(todoZadatak.getDatumZavrsetka());
            /*Picasso.get()
                    .load("http://becomingminimalist.com/wp-content/uploads/2008/07/post-it-note.jpg?w=300")
                    .resize(30, 30)
                    .into(imageViewRV);*/
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRVTODOClicked(todoZadatak);
                }
            });
        }
    }

    public RVAdapterTODO(List<TODOZadatak> todoLista, OnRVTODOClickedListener listener) {
        this.listaTODO = todoLista;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RVAdapterTODO.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_todo_single_item, viewGroup, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RVAdapterTODO.MyViewHolder myViewHolder, int i) {
        myViewHolder.bind(listaTODO.get(i), listener);
        int currentPostition = i;
        final TODOZadatak todoZadatak = listaTODO.get(currentPostition);
        if (todoZadatak.getStatus().equalsIgnoreCase("Zavrsen")) {
            myViewHolder.view.setBackgroundColor(Color.GRAY);
        } else if (todoZadatak.getStatus().equalsIgnoreCase("Aktivan")) {
            if (todoZadatak.getPrioritet().equalsIgnoreCase("Nizak")) {
                myViewHolder.view.setBackgroundColor(Color.YELLOW);
            } else if (todoZadatak.getPrioritet().equalsIgnoreCase("Normalan")) {
                myViewHolder.view.setBackgroundColor(Color.GREEN);
            } else if (todoZadatak.getPrioritet().equalsIgnoreCase("Visok")) {
                myViewHolder.view.setBackgroundColor(Color.RED);
            }
        }
    }


    @Override
    public int getItemCount() {
        return listaTODO.size();
    }
}
